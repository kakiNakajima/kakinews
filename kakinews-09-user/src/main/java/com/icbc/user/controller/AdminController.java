package com.icbc.user.controller;

import com.icbc.common.StatusCode;
import com.icbc.entity.PageResult;
import com.icbc.entity.Result;
import com.icbc.user.pojo.Admin;
import com.icbc.user.service.AdminService;
import com.icbc.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * admin控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private JwtUtil jwtUtil;

	@PostMapping("/login")
	public Result login(@RequestBody Admin admin){
		if (admin.getLoginname()==null){
			return new Result(false, StatusCode.USER_PASS_ERROR, "登录失败，用户名或者密码错误");
		}

		Admin dbadmin = adminService.findByLoginname(admin.getLoginname());

		/**
		 * matches(参数一：明文密码,参数二：bciypt加密后的密码)
		 *
		 */
		if (dbadmin!=null && encoder.matches(admin.getPassword(), dbadmin.getPassword())){

			//用户名与密码都没错时签发token
			String token = jwtUtil.createJWT(dbadmin.getId(), dbadmin.getLoginname(), "admin");
			HashMap<String, Object> hashMap = new HashMap<>();
			hashMap.put("name", dbadmin.getLoginname());
			hashMap.put("Id",dbadmin.getId());
			hashMap.put("token",token);


			return new Result(true, StatusCode.OK, "登录成功",hashMap);
		}



		return new Result(false, StatusCode.USER_PASS_ERROR, "登录失败，用户名或者密码错误");


	}
	/**
	 * 查询全部数据
	 * @return
	 */
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true, StatusCode.OK,"查询成功",adminService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",adminService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<Admin> pageList = adminService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Admin>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",adminService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param admin
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Admin admin  ){
		//新增用户时加密密码
		admin.setPassword(encoder.encode(admin.getPassword()));
		adminService.add(admin);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param admin
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Admin admin, @PathVariable String id ){
		admin.setId(id);
		adminService.update(admin);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		adminService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}
	
}
