package test;

import io.jsonwebtoken.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateJwt {
    public static void main(String[] args) {
        JwtBuilder builder = Jwts.builder();

        //封装jwttoken
        JwtBuilder builder1 = builder.setId("0049119")
                .setIssuedAt(new Date())
                .setSubject("kaki")
                .signWith(SignatureAlgorithm.HS256, "admin");

        //契约，生成token
        String token = builder1.compact();
        //eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwMDQ5MTE5IiwiaWF0IjoxNTgwMzY1MjM5LCJzdWIiOiJrYWtpIn0.cXbbnpdNFajt-MHellFJV5pCDucWy3lY1qk6xVciC5c
        System.out.println("解密前："+token);
        parseTest(token);



    }


    //解密tonken
    public static void  parseTest( String token){

        Jws<Claims> tokenparser = Jwts.parser().setSigningKey("admin").parseClaimsJws(token);

        Claims tokenparserBody = tokenparser.getBody();

        String id = tokenparserBody.getId();
        Date date = tokenparserBody.getIssuedAt();
        String username = tokenparserBody.getSubject();

        System.out.println("解密后：id:"+id+",date:"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date)+",username:"+username);

    }
}
