package com.icbc.dao;

import com.icbc.pojo.Spit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * 吐槽jap接口
 */
public interface SpitDao extends MongoRepository<Spit,String>{

    /**
     * 查询吐槽（不包含评论）
     * @return
     */
    List<Spit> findByParentidIsNull();

    /**
     * 根据父id查询评论
     * @param parentid
     * @param of
     * @return
     */
    Page<Spit> findByParentid(String parentid, Pageable of);


    Page<Spit> findAll(Pageable of);
}
