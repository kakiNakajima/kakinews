# kakinews ,Kaki新闻，Kaki博客

#### 介绍
KAKI新闻，Kaki博客

#### 软件架构
软件架构说明

  1.后端整体布局采用springcloud芬奇利2.0.0RC1，SSS模式 springCloud,springBoot,springData家族   	的部分成员MongoDB,JPA等，前后交接才有JWT进行通讯，前端通过ZUUL网关访问记性访问，部	分板块使用爬虫（webmagic by HuangYiHua）定时爬取相关数据，进行模拟更新,持续更新中，后	续会使用搜索引擎优化查询功能

  2.前台web 基于vue2.0 采用nuxt服务器渲染,vue-infinite-scroll瀑布流，vue-quill-editor富文本，   	   	overtrue-share分享，js-cookie等开源框架。

  3.后台admin 使用vueAdmin-template-master脚手架(未前后交接)


#### 安装教程

1.  后端推荐使用idea         (确保配置的都对得上就可以直接用启动类，建议先启动Eureka)
2.  前台推荐使用vscode  （拉取到本地 nmp intall  ,npm run dev）

#### 使用说明

1.  后端使用idea，spring工程即可
2.  前台在使用之前删除package-lock.json，使用npm install  ,然后使用npm run dev,可以先用easy-mock来模拟后台接口,也可以直接用后端服务

<h4>前台效果图：</h4>

  首页

![1](/vue/kakinews-web/assets/img/jieshao/1.png)

博文专栏

![Snipaste_2020-03-29_15-31-57](/vue/kakinews-web/assets/img/jieshao/Snipaste_2020-03-29_15-31-57.png)

吐槽板块：

![Snipaste_2020-03-29_15-36-35](/vue/kakinews-web/assets/img/jieshao/Snipaste_2020-03-29_15-36-35.png)

![Snipaste_2020-03-29_15-37-27](/vue/kakinews-web/assets/img/jieshao/Snipaste_2020-03-29_15-37-27.png)

社交板块（开发中）：

![jiaoyou](/vue/kakinews-web/assets/img/jieshao/jiaoyou.png)

活动板块：

![huodong](/vue/kakinews-web/assets/img/jieshao/huodong.png)

![huodongxiangqing](/vue/kakinews-web/assets/img/jieshao/huodongxiangqing.png)

以及登录和注册等

![login](/vue/kakinews-web/assets/img/jieshao/login.png)

注：本系统比较庞大，尚在开发中，以上介绍之后前台的一些页面，管理后台目前还未做到前后交接。当前支撑需6个微服务，以及前台的nodejs

![cloud](/vue/kakinews-web/assets/img/jieshao/cloud.png)

如果只想搭建个人博客建议缩减为1到2个微服务，前台采用一台nginx部署nuxt文件即可

<font size="5px">作者心语</font>

 1.该系统可用于博客，新闻等web网站，也可以用于教学，或者拓展些其他项目。由于微服务的方便	简洁您可以随心所欲的驾驭它。

  2.本人无任何目的，只是爱好，如有遇到相关问题可在下方留言或者加QQ群699816271 进行互相学 	习，技术就应该互相分享

  3.本系统也是基于某些网站，采取的素材，如有侵权，请及时联系删帖。

#### 参与贡献

1. Fork 本仓库

2. 提交代码

3. 新建 Pull Request

4. 本项目设计者cfei_net,基于1024平台（私人平台），再次基础上进行改造

5. 注：该项目目前还在开发阶段，尚未进行接口对接，后端服务未做周密优化，只是试验阶段

   所以代码下载到本地后还要进行二度开发


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
