/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.7.21 : Database - tensquare_user
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tensquare_user` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tensquare_user`;

/*Table structure for table `tb_admin` */

DROP TABLE IF EXISTS `tb_admin`;

CREATE TABLE `tb_admin` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `loginname` varchar(100) DEFAULT NULL COMMENT '登陆名称',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `state` varchar(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员';

/*Data for the table `tb_admin` */

insert  into `tb_admin`(`id`,`loginname`,`password`,`state`) values 
('1226409224247037952','admin','$2a$10$uw5lEAFiO4C/KrnIcJ3/muC/8BrHi96rKVyi/Z.urV5CN0IBxv7v6',NULL);

/*Table structure for table `tb_follow` */

DROP TABLE IF EXISTS `tb_follow`;

CREATE TABLE `tb_follow` (
  `userid` varchar(20) NOT NULL COMMENT '用户ID',
  `targetuser` varchar(20) NOT NULL COMMENT '被关注用户ID',
  PRIMARY KEY (`userid`,`targetuser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_follow` */

insert  into `tb_follow`(`userid`,`targetuser`) values 
('1','1'),
('1','10');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `loginname` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `birthday` datetime DEFAULT NULL COMMENT '出生年月日',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  `email` varchar(100) DEFAULT NULL COMMENT 'E-Mail',
  `regdate` datetime DEFAULT NULL COMMENT '注册日期',
  `updatedate` datetime DEFAULT NULL COMMENT '修改日期',
  `lastdate` datetime DEFAULT NULL COMMENT '最后登陆日期',
  `online` bigint(20) DEFAULT NULL COMMENT '在线时长（分钟）',
  `interest` varchar(100) DEFAULT NULL COMMENT '兴趣',
  `personality` varchar(100) DEFAULT NULL COMMENT '个性',
  `fanscount` int(20) DEFAULT NULL COMMENT '粉丝数',
  `followcount` int(20) DEFAULT NULL COMMENT '关注数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`mobile`,`loginname`,`password`,`nickname`,`sex`,`birthday`,`avatar`,`email`,`regdate`,`updatedate`,`lastdate`,`online`,`interest`,`personality`,`fanscount`,`followcount`) values 
('1',NULL,'testuser','111111','小白','男','2018-01-08 15:39:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0),
('1223561441118904320',NULL,NULL,NULL,'Jeremy_pan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
('1223563170828902400',NULL,NULL,NULL,'华山猛男',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
('1223563170849873920',NULL,NULL,NULL,'临江仙卜算子',NULL,NULL,'1223561505597878272.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
('1223563919050711040',NULL,'随风溜达的向日葵',NULL,'随风溜达的向日葵',NULL,NULL,NULL,NULL,'2020-02-01 19:08:52','2020-02-01 19:08:52','2020-02-01 19:08:52',NULL,NULL,NULL,NULL,NULL),
('1223563940982726656',NULL,'Java技术栈',NULL,'Java技术栈',NULL,NULL,NULL,NULL,'2020-02-01 19:09:00','2020-02-01 19:09:00','2020-02-01 19:09:00',NULL,NULL,NULL,NULL,NULL),
('1223564032619880448',NULL,'李长春',NULL,'李长春',NULL,NULL,'1223563931373535232.jpg',NULL,'2020-02-01 19:09:09','2020-02-01 19:09:09','2020-02-01 19:09:09',NULL,NULL,NULL,NULL,NULL),
('1223564043927724032',NULL,'大王叫下',NULL,'大王叫下',NULL,NULL,NULL,NULL,'2020-02-01 19:09:21','2020-02-01 19:09:21','2020-02-01 19:09:21',NULL,NULL,NULL,NULL,NULL),
('1223564043944501248',NULL,'a伟正是在下',NULL,'a伟正是在下',NULL,NULL,NULL,NULL,'2020-02-01 19:09:30','2020-02-01 19:09:30','2020-02-01 19:09:30',NULL,NULL,NULL,NULL,NULL),
('1223564070477668352',NULL,'Airship',NULL,'Airship',NULL,NULL,'1223564069030592512.jpg',NULL,'2020-02-01 19:09:41','2020-02-01 19:09:41','2020-02-01 19:09:41',NULL,NULL,NULL,NULL,NULL),
('1223564095446360064',NULL,'呐呐丶嘿',NULL,'呐呐丶嘿',NULL,NULL,'1223564094091558912.jpg',NULL,'2020-02-01 19:09:47','2020-02-01 19:09:47','2020-02-01 19:09:47',NULL,NULL,NULL,NULL,NULL),
('1223564121740451840',NULL,'mo311',NULL,'mo311',NULL,NULL,NULL,NULL,'2020-02-01 19:09:54','2020-02-01 19:09:54','2020-02-01 19:09:54',NULL,NULL,NULL,NULL,NULL),
('1223564274421506048',NULL,'吴伟祥',NULL,'吴伟祥',NULL,NULL,'1223564273309974528.jpg',NULL,'2020-02-01 19:10:30','2020-02-01 19:10:30','2020-02-01 19:10:30',NULL,NULL,NULL,NULL,NULL),
('1223564301453795328',NULL,'周立_ITMuch',NULL,'周立_ITMuch',NULL,NULL,NULL,NULL,'2020-02-01 19:10:37','2020-02-01 19:10:37','2020-02-01 19:10:37',NULL,NULL,NULL,NULL,NULL),
('1223564325164195840',NULL,'小刀爱编程',NULL,'小刀爱编程',NULL,NULL,NULL,NULL,'2020-02-01 19:10:42','2020-02-01 19:10:42','2020-02-01 19:10:42',NULL,NULL,NULL,NULL,NULL),
('1223564349256278016',NULL,'神是到着念',NULL,'神是到着念',NULL,NULL,'1223564348329295872.jpg',NULL,'2020-02-01 19:10:48','2020-02-01 19:10:48','2020-02-01 19:10:48',NULL,NULL,NULL,NULL,NULL),
('1223564373499355136',NULL,'little_su',NULL,'little_su',NULL,NULL,'1223564372463321088.jpg',NULL,'2020-02-01 19:10:54','2020-02-01 19:10:54','2020-02-01 19:10:54',NULL,NULL,NULL,NULL,NULL),
('1223564398564515840',NULL,'睡懒觉的猫',NULL,'睡懒觉的猫',NULL,NULL,'1223564397322960896.jpg',NULL,'2020-02-01 19:11:00','2020-02-01 19:11:00','2020-02-01 19:11:00',NULL,NULL,NULL,NULL,NULL),
('1244143284557742080','111','kaki','$2a$10$W.JbYblF4NHQhjgijxbeue6ixE4eDOnctJRSsWgKXyJ0Nc2H65IxC','kaki',NULL,NULL,'https://portrait.gitee.com/uploads/avatars/user/1720/5160819_kakiNakajima_1578981461.png!avatar200',NULL,'2020-03-29 14:04:16',NULL,NULL,0,NULL,NULL,0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
