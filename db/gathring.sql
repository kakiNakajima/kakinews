/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.7.21 : Database - tensquare_gathering
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tensquare_gathering` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tensquare_gathering`;

/*Table structure for table `tb_gathering` */

DROP TABLE IF EXISTS `tb_gathering`;

CREATE TABLE `tb_gathering` (
  `id` varchar(20) NOT NULL COMMENT '编号',
  `name` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `summary` text COMMENT '大会简介',
  `detail` text COMMENT '详细说明',
  `sponsor` varchar(100) DEFAULT NULL COMMENT '主办方',
  `image` varchar(100) DEFAULT NULL COMMENT '活动图片',
  `starttime` datetime DEFAULT NULL COMMENT '开始时间',
  `endtime` datetime DEFAULT NULL COMMENT '截止时间',
  `address` varchar(100) DEFAULT NULL COMMENT '举办地点',
  `enrolltime` datetime DEFAULT NULL COMMENT '报名截止',
  `state` varchar(1) DEFAULT NULL COMMENT '是否可见',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动';

/*Data for the table `tb_gathering` */

insert  into `tb_gathering`(`id`,`name`,`summary`,`detail`,`sponsor`,`image`,`starttime`,`endtime`,`address`,`enrolltime`,`state`,`city`) values 
('1','测试活动','喝茶看电影，不亦乐乎','喝茶看电影，不亦乐乎','某甲','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00',NULL,NULL,'1','1'),
('94377594140','906地区活动','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><h3><strong>| SOFAChannel</strong></h3> \r\n<p>&lt;SOFA:Channel/&gt; 有趣实用的分布式架构频道，前沿技术、直播 Coding、观点“抬杠”，多种形式</p> \r\n<p>&lt;SOFA:Channel/&gt; 将作为 SOFA 所有在线内容的承载，包含直播/音视频教程，集中体现 SOFAStack 的能力全景图。</p> \r\n<h3><strong>|&nbsp;</strong><strong>SOFAChannel#14：</strong><strong>云原生网络代理&nbsp;</strong><strong>MOSN 的扩展机制解析</strong></h3> \r\n<p>MOSN 是一款使用 Go 语言开发的网络代理软件，由蚂蚁金服开源并经过几十万容器的生产级验证。</p> \r\n<p>MOSN&nbsp;作为云原生的网络数据平面，旨在为服务提供多协议，模块化，智能化，安全的代理能力。在实际的生产使用场景中，通用的网络代理总会与实际业务定制需求存在差异，MOSN 提供了一系列可编程的扩展机制，就是为了解决这种场景。</p> \r\n<p>本次分享将向大家介绍 MOSN 的扩展机制解析以及一些扩展实践的案例。</p> \r\n<p>欢迎先下载 Demo，提前体验 MOSN 拓展机制的使用，成功完成预习作业—— Demo 完整操作的，有机会获得小礼物哟（记得留下完成的证明，获得方式在直播中公布），我们将在直播中公布答案——进行 Demo 的详细演示。PS：在直播中也会发布闯关任务，完成闯关任务也有机会获得小礼物哟～</p> \r\n<p>Demo：</p> \r\n<p><a href=\"https://github.com/mosn/mosn/tree/master/examples/codes/mosn-extensions\" target=\"_blank\" rel=\"nofollow\">https://github.com/mosn/mosn/tree/master/examples/codes/mosn-extensions</a></p> \r\n<p>Demo Readme：</p> \r\n<p><a href=\"https://github.com/mosn/mosn/tree/master/examples/cn_readme/mosn-extensions\" target=\"_blank\" rel=\"nofollow\">https://github.com/mosn/mosn/tree/master/examples/cn_readme/mosn-extensions</a></p> \r\n<p>欢迎了解 MOSN：<a href=\"https://github.com/mosn/mosn\" target=\"_blank\" rel=\"nofollow\">https://github.com/mosn/mosn</a></p> \r\n<p><strong>针对人群：</strong></p> \r\n<p>对云原生、Service Mesh、网络代理有基本了解，想要了解云原生以及对云原生网络代理 MOSN 有二次开发需求的人群</p> \r\n<p><strong>你将收获：</strong></p> \r\n<ul> \r\n <li>快速了解 MOSN 的多种扩展能力</li> \r\n <li>3 个案例，实际体验 MOSN 扩展能力</li> \r\n <li>多案例多形式，使用 MOSN 实现个性化业务需求</li> \r\n</ul> \r\n<p><strong>| 加入 SOFA 钉钉互动群</strong></p> \r\n<p>欢迎加入直播互动钉钉群：30315793（搜索群号加入即可）</p> \r\n<p><img alt=\"\" width=\"100%\"  src=\"https://static.oschina.net/uploads/space/2020/0325/160327_Acg8_4081603.jpg\" width=\"200\"></p><p></p>\r\n                </div>',NULL,NULL,'http://www.cfei.net/wp-content/uploads/2016/11/2016112914283054.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00',NULL,NULL,'1','1'),
('943776146707845','琶洲艺术会展','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p style=\"text-align: left;\">全球敏捷运维峰会（Gdevops2020），是国内同时覆盖一线与二线城市的高端技术峰会，在各地均有极大影响力。目前已成功巡回北京、上海、广州、杭州、成都五城，主题覆盖敏捷运维、数据库、云与架构等重点方向。Gdevops峰会汇聚dbaplus社群数百专家资源，是与政府、企业携手打造的敏捷运维领域标杆盛会，全面覆盖从DBA、运维工程师到CXO等所有技术圈层、从互联网、电信、金融、交通到物流等重点行业，在业界、媒体界具有极大影响力。</p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113253_VPtZ_2322411.png\"></p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113312_OSlU_2322411.png\"></p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113507_oGhK_2322411.png\"></p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113526_DPbE_2322411.png\"></p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113600_q0qr_2322411.png\"></p> \r\n<p style=\"text-align: center;\"><img alt=\"\" src=\"https://static.oschina.net/uploads/space/2020/0219/113701_eHIk_2322411.png\"></p><p></p>\r\n                </div>',NULL,'ssss','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','cccc',NULL,'1','1'),
('943776663576121344','天河码农协会聚会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,NULL,'http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00',NULL,NULL,'1','2'),
('943783521749700608','海珠区码农年会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'23454534','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','545435435',NULL,'1','2'),
('944085821768732672','JAVAEE茶话会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'阿智','http://www.cfei.net/wp-content/uploads/2016/11/2016112914283054.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','金燕龙',NULL,'1','2'),
('944086086991351808','番禺Code代表大会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'11','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','11',NULL,'1','3'),
('944090372710207488','光谷社区','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'小马','http://www.cfei.net/wp-content/uploads/2016/11/2016112914283054.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','消息',NULL,'1','3'),
('944105652622594048','世田谷大会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'测试者','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','测试地址',NULL,'1','4'),
('945227340642914304','广大松田67届学生会','<div class=\"event-detail editor-viewer\">\r\n                                        <p></p><p>在本次网络研讨会中，您将了解&nbsp;Kubernetes&nbsp;和&nbsp;Elastic&nbsp;的新功能—特别是在简化操作和&nbsp;Elastic&nbsp;可观测性体验方面。我们将讨论&nbsp;Kubernetes&nbsp;操作模式以及&nbsp;Elastic&nbsp;Cloud&nbsp;on&nbsp;Kubernetes(ECK)&nbsp;是如何简化部署和管理的。最后，我们将介绍Elastic&nbsp;Stack&nbsp;的可观测性如何使您能够更好地了解应用堆栈，从而诊断、调试和解决应用中的问题。</p> \r\n<p>&nbsp;</p> \r\n<p><strong>亮点：</strong></p> \r\n<ul> \r\n <li>Elastic在CNCF云原生交互场景中的位置</li> \r\n <li>在Kubernetes上运行&nbsp;Elastic&nbsp;Stack</li> \r\n <li>演示：如何使用&nbsp;ECK&nbsp;部署、防护和升级&nbsp;Elasticsearch</li> \r\n <li>Beats&nbsp;中使用&nbsp;autodiscover&nbsp;监测动态工作负载的入门知识</li> \r\n <li>在Kubernetes上掌握可观测经验</li> \r\n</ul> \r\n<p><a href=\"http://ossh5.rockstudio.cn/Elastic/minisite/detailact.html?id=8&amp;refer=do\" rel=\"nofollow\">点击报名</a></p><p></p>\r\n                </div>',NULL,'222','http://www.cfei.net/wp-content/uploads/2016/11/2016112914314664.jpg','2020-03-02 18:05:53','2020-12-21 00:00:00','333',NULL,'1','5');

/*Table structure for table `tb_usergath` */

DROP TABLE IF EXISTS `tb_usergath`;

CREATE TABLE `tb_usergath` (
  `userid` varchar(20) NOT NULL COMMENT '用户ID',
  `gathid` varchar(20) NOT NULL COMMENT '活动ID',
  `exetime` datetime DEFAULT NULL COMMENT '点击时间',
  PRIMARY KEY (`userid`,`gathid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户关注活动';

/*Data for the table `tb_usergath` */

insert  into `tb_usergath`(`userid`,`gathid`,`exetime`) values 
('1','200','2018-01-06 15:44:04');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
