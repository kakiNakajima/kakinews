package com.icbc.task;

import com.icbc.pipeline.ArticlePipeline;
import com.icbc.processor.ArticlePageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.RedisScheduler;

/**
 * 文章爬虫定时任务类
 */
@Component
public class ArticleTask {

    /**
     * 爬虫处理类
     */
    @Autowired
    private ArticlePageProcessor articlePageProcessor;

    /**
     * 爬虫入库类
     */
    @Autowired
    private ArticlePipeline articlePipeline;

    /**
     * 爬虫记录重复类（redis模式）
     */
    @Autowired
    private RedisScheduler scheduler;


    @Scheduled(cron = "10 59 14 * * ?")
    public void task1(){

        //开始蜘蛛侠模式
        Spider.create(articlePageProcessor)                //处理器
                .addUrl("https://www.oschina.net/blog")    //目标
                .setScheduler(scheduler)                   //去重
                .addPipeline(articlePipeline)              //持久化
                .start();                                  //go go go！
    }
}
