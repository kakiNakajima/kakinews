package com.icbc.pipeline;

import com.icbc.cliens.ArticleClient;
import com.icbc.dto.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.Date;

/**
 * 文章爬取入库类
 * @author cfei
 * @author kaki
 */
@Component
public class ArticlePipeline implements Pipeline {

    @Autowired
    private ArticleClient articleClient;

    @Override
    public void process(ResultItems resultItems, Task task) {
        Article article = new Article();

        article.setTitle(resultItems.get("title"));
        article.setContent(resultItems.get("content"));
        article.setCreatetime(new Date());
        article.setState("1");
        article.setVisits(0);

        articleClient.add(article);
    }
}
