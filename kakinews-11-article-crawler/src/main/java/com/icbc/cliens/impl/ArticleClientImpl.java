package com.icbc.cliens.impl;

import com.icbc.cliens.ArticleClient;
import com.icbc.common.StatusCode;
import com.icbc.dto.Article;
import com.icbc.entity.Result;
import org.springframework.stereotype.Component;

@Component
public class ArticleClientImpl implements ArticleClient {
    @Override
    public Result add(Article article) {
        return new Result(false, StatusCode.REMOTE_ERROR, "远程调用文章微服务失败");
    }
}
