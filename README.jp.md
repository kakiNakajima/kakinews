# kakinews

#### 紹介する
KAKI新闻（カキーニュース）

著者　カキーじょ

#### ソフトウェア　建築
　説明する

1.  ベックはJavaを使い、springcloud　Finchley2.0.0RC1
2.  web はvue2.0を使う
3.  web－admin　はvueAdmin-template-masterを使うインスタント麵

<h4>インストール</h4>

1.  ベックはidea
2.  webはvscode 

#### 使いかた

1.  中国語のREADMEをご覧ください

## debug方法?

`global`に`debug`を設定か, `haproxy`を`-d`で起動する。

キャッシュに関するメッセージは`[CACHE]`を含む。

## どうやってPOSTリクエストをキャッシュする?

`option http-buffer-request`を設定

カスタマイズしたkeyは`body`を入れること。

POST bodyは不完全な可能性があるので、**option http-buffer-request** section in [HAProxy configuration](https://gitee.com/XlSmallMing/nuster/blob/master/doc/configuration.txt) を参照

単独でPOST用のbackendを設置した方がいいかもしれない

<h3><font color=gree>ディレクティブ</font></h3>

1.  貴方は码云で Readme\_XXX.md  Readme\_en.md, Readme\_zh.md　言語の変わり事が出来る
2.  blog は　[blog.gitee.com](https://blog.gitee.com)　を訪問してください。
3.  他のプログラムはここに [https://gitee.com/explore](https://gitee.com/explore)
4.  [GVP](https://gitee.com/gvp)　は最高評価です
5.  Gitee説明はここ　 [https://gitee.com/help](https://gitee.com/help)
6.  Giteeホームページは素晴らしいプログラムを展示する  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
