package com.icbc.qa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.icbc.qa.pojo.Problem;
import org.springframework.data.jpa.repository.Query;

/**
 * problem数据访问接口
 * @author Administrator
 *
 */
public interface ProblemDao extends JpaRepository<Problem,String>,JpaSpecificationExecutor<Problem>{

    //最新问答
    @Query(value = "select * from tb_problem t1,tb_pl t2 where t1.id = t2.problemid and t2.labelid = ?1 order by t1.replytime",nativeQuery = true)
    Page<Problem> newList(String label, Pageable of);

    //最热问答
    @Query(value = "select * from `tb_problem` t1,`tb_pl` t2 where t1.`id` = t2.`problemid` and t2.labelid = ?1 order by t1.`reply` desc ",nativeQuery = true)
    Page<Problem> hotList(String label, Pageable of);

    //等待回答
    @Query(value = "select * from `tb_problem` t1,`tb_pl` t2 where t1.`id` = t2.`problemid` and t2.labelid = ?1 and t1.`reply` = 0 ",nativeQuery = true)
    Page<Problem> waitlist(String label, Pageable of);
}
