package com.icbc.controller;

import com.icbc.common.StatusCode;
import com.icbc.entity.Label;
import com.icbc.entity.PageResult;
import com.icbc.entity.Result;
import com.icbc.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin  //跨域注解
@RestController
@RequestMapping("/label")
public class LabelController {

    @Autowired
    private LabelService labelService;

    //查询所有
    @GetMapping
    public Result findAll(){
        return new Result(true, StatusCode.OK, "查询全部标签成功",labelService.findAll());
    }
    //查询一个
    @GetMapping("/{id}")
    public Result findById(@PathVariable String id){
        return new Result(true, StatusCode.OK, "查询一个标签成功",labelService.findById(id));
    }
    //新增一个
    @PostMapping
    public Result seveLabel(@RequestBody Label label){
        labelService.seveLabel(label);
        return new Result(true, StatusCode.OK, "新增成功");
    }
    //修改一个
    @PutMapping("/{id}")
    public Result updateLabel(@PathVariable String id,@RequestBody Label label){
        labelService.updateLabel(id, label);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    //删除一个
    @DeleteMapping("/{id}")
    public Result deleteLabel(@PathVariable String id){
        labelService.deleteLabel(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    //条件查询
    @PostMapping("/search")
    public Result search(@RequestBody Label label){
        List<Label> list = labelService.search(label);
        return new Result(true, StatusCode.OK, "条件查询成功",list);
    }

    //条件分页查询
    @PostMapping("/search/{page}/{size}")
    public Result searchPage(@PathVariable("page") int page,@PathVariable("size") int size, @RequestBody Label label){
        Page<Label> res = labelService.search(page,size,label);
        return new Result(true,
                StatusCode.OK,
                "条件查询成功",
                new PageResult<Label>(res.getTotalElements(),res.getContent()));
    }
}
