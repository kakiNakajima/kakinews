package com.icbc.service;

import com.icbc.dao.LabelDao;
import com.icbc.entity.Label;
import com.icbc.utils.IdWorker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 标签服务
 */
@Service
public class LabelService {

    @Autowired
    private LabelDao labelDao;

    @Autowired
    private IdWorker idWorker;


    //查询所有
    public List<Label> findAll(){
        return labelDao.findAll();
    }
    //查询一个
    public Label findById(String id){
        return labelDao.findById(id).get();
    }
    //新增一个
    @Transactional
    public void seveLabel(Label label){
        label.setId(idWorker.nextId()+"");
        labelDao.save(label);
    }
    //修改一个
    @Transactional
    public void updateLabel(String id,Label label){
        label.setId(id);
        labelDao.save(label);
    }

    //删除一个
    @Transactional
    public void deleteLabel(String id){
        labelDao.deleteById(id);
    }


    //条件查询
    public List<Label> search(Label label){

        //创建动态查询条件对象
        Specification<Label> spec =crateSpecification(label);


        List<Label> list = labelDao.findAll(spec);

        return list;

    }

    //条件分页查询
    public Page<Label> search(int page, int size, Label label) {
        //创建动态查询条件对象
        Specification<Label> spec =crateSpecification(label);

        Page<Label> res = labelDao.findAll(spec, PageRequest.of(page, size));

        return res;
    }

    private Specification crateSpecification(Label label){
        return  new Specification<Label>() {

            /**
             * 查询条件组建
             * @param root 源条件
             * @param criteriaQuery
             * @param cb 条件构建者
             * @return
             */
            @Override
            public Predicate toPredicate(Root<Label> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                ArrayList<Predicate> plist = new ArrayList<>(0);
                //标签名称模糊匹配
                if(StringUtils.isNoneBlank(label.getLabelname())){
                    Predicate labelname = cb.like(root.get("labelname"), "%"+label.getLabelname()+"%");
                    plist.add(labelname);
                }
                if(StringUtils.isNoneBlank(label.getState())){
                    Predicate state = cb.equal(root.get("state"), label.getState());
                    plist.add(state);
                }
                Predicate[] preArr = new Predicate[plist.size()];
                plist.toArray(preArr);

                return cb.and(preArr);
            }
        };
    }
}
