package com.icbc.entity;

/**
 * 响应实体类
 * @author Kaki Nakajima
 */
public class Result {

    private Integer code;
    private Boolean flag;
    private String message;
    private Object data;

    /**
     * 无参构造器
     */
    public Result() {
        super();
    }

    /**
     * 有参的构造器
     * @param flag
     * @param code
     * @param message
     */
    public Result(boolean flag, int code, String message) {
        super();
        this.code = code;
        this.flag = flag;
        this.message = message;
    }

    /**
     * 全参的构造器
     * @param code
     * @param flag
     * @param message
     * @param data
     */
    public Result(boolean flag, int code, String message, Object data) {
        super();
        this.code = code;
        this.flag = flag;
        this.message = message;
        this.data = data;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
