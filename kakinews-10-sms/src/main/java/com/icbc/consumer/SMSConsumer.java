package com.icbc.consumer;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.icbc.utils.SmsUtil;
import org.apache.commons.logging.Log;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Map;

@Component
@RabbitListener(queues = "kakinews_user_sms")
public class SMSConsumer {

    @Autowired
    private SmsUtil smsUtil;

    @Autowired
    private Log log;


    @Value("${aliyun.sms.template_code}")
    private String templateCode;

    @Value("${aliyun.sms.sign_name}")
    private String signName;

    @RabbitHandler
    public void userSendMessage(Map<String ,Object> map){

        String code = (String) map.get("code");
        String mobile= (String)map.get("mobile");
        String param = "{\"code\":\""+code+"\"}";

        try {
            SendSmsResponse sendSmsResponse = smsUtil.sendSms(mobile, templateCode, signName, param);
        }catch (Exception e){
            e.printStackTrace();
            log.info("短信发送失败:" +"\n"+
                          "\t\t手机号："+mobile+"\n"+
                          "\t\t 时间："+new SimpleDateFormat("yyyy-MM-dd HH:ss:mm").format(System.currentTimeMillis())+"\n"+
                          "\t\t 原因："+e.getMessage()
            );
            System.out.println("短信发送失败");
        }


    }
}
