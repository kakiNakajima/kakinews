import request from '@/utils/request'

export default {
  // 获取城市列表
  cityList() {
    return request({
      url: `/base/city`,
      method: 'get'
    })
  }
}
