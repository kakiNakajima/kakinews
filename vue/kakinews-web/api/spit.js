import request from '@/utils/request'

// 优化代码：与微服务网关同步
const group_name = 'spit'
const api_name = 'spit'

export default {
    
    search(curPage,size,searchEntity){
        return request({
            url: `${group_name}/${api_name}/search/${curPage}/${size}`,
            method: 'post',
            data: searchEntity
        })
    },
    findById(id){
        return  request({
            url: `${group_name}/${api_name}/${id}`,
            method: 'get'
        })
    },
    findCommentList(parentid,curPage,size){
        return  request({
            url:  `${group_name}/${api_name}/common/${parentid}/${curPage}/${size}`,
            method: 'get'
        })
    },
    //吐槽点赞
    thumbup(id){
        return request({
            url : `/${group_name}/${api_name}/thumbup/${id}`,
            method: 'put'
        })
    },
    save(pojo){
        return  request({
            url : `/${group_name}/${api_name}`,
            method: 'post',
            data: pojo
        })
    }


}