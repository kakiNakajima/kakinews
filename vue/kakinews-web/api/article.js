import request from '@/utils/request'

const group_name = 'article'
const api_name = 'article'

export default {
  findById(id) {
    return request({
      url: `/${group_name}/${api_name}/${id}`,
      method: 'get'
    })
  },
  //分页查询文章
  searchlist(page,size,data) {
    return request({
      url: `/${group_name}/${api_name}/search/${page}/${size}`,
      method: 'post',
      data:data
    })
  }
}
