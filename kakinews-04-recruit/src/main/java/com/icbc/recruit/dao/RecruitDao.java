package com.icbc.recruit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.icbc.recruit.pojo.Recruit;

import java.util.List;

/**
 * recruit数据访问接口
 * @author Administrator
 *
 */
public interface RecruitDao extends JpaRepository<Recruit,String>,JpaSpecificationExecutor<Recruit>{

    /**
     * 最新职位
     * @param s
     * @return
     */
    List<Recruit> findTop12ByStateNotOrderByCreatetimeDesc(String s);

    /**
     * 推荐职位
     * @param s
     * @return
     */
    List<Recruit> findTop4ByStateOrderByCreatetimeDesc(String s);
}
