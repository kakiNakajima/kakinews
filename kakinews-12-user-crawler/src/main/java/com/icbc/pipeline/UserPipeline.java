package com.icbc.pipeline;

import com.icbc.cliens.UserClient;
import com.icbc.dto.User;
import com.icbc.utils.DownloadUtil;
import com.icbc.utils.IdWorker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.Date;

/**
 * 用户爬虫入库类
 */
@Component
public class UserPipeline implements Pipeline {

    @Autowired
    private UserClient userClient;

    @Autowired
    private IdWorker idWorker;
    @Override
    public void process(ResultItems resultItems, Task task) {
        User user = new User();

        String nickname = resultItems.get("nickname");

        String avatar = resultItems.get("avatar");

        user.setNickname(nickname);

        user.setLoginname(nickname);
        Date date = new Date();
        user.setRegdate(date);
        user.setUpdatedate(date);
        user.setLastdate(date);

        String fileName = "";
        if(StringUtils.isNotBlank(avatar)&&avatar.length()>0){
            try {
                fileName = idWorker.nextId() +".jpg";
                DownloadUtil.download(avatar, fileName,"D:\\temp\\crawler-avatar");
            }catch (Exception e){
                System.out.println("图片下载失败:"+e.getMessage());
                fileName = "";
            }

        }

        if(fileName.length()>0){
            user.setAvatar(fileName);
        }


        userClient.add(user);
    }
}
