package com.icbc.task;

import com.icbc.pipeline.UserPipeline;
import com.icbc.processor.UserPageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.RedisScheduler;

@Component
public class UserCrawlerTask {

    @Autowired
    private UserPageProcessor userPageProcessor;

    @Autowired
    private UserPipeline userPipeline;

    @Autowired
    private RedisScheduler redisScheduler;


    @Scheduled(cron = "40 8 19 * * ?")
    public void task1(){
        Spider.create(userPageProcessor)                 //爬虫处理类
                .addUrl("https://www.oschina.net/blog")  //url
                .addPipeline(userPipeline)               //爬虫入库类
                .setScheduler(redisScheduler)            //去重类
                .start();                                //启动
    }
}
