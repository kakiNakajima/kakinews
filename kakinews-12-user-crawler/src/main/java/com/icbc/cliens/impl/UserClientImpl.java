package com.icbc.cliens.impl;

import com.icbc.cliens.UserClient;
import com.icbc.common.StatusCode;
import com.icbc.dto.User;
import com.icbc.entity.Result;
import org.springframework.stereotype.Component;

/**
 * 熔断类
 */
@Component
public class UserClientImpl implements UserClient {
    @Override
    public Result add(User user) {
        return new Result(false, StatusCode.REMOTE_ERROR, "用户数据采集失败");
    }
}
